const request = require('request');
const scrapeIt = require('scrape-it');
const jsonfile = require('jsonfile');

var file = 'data.json';

var options = 'http://en.ostranah.ru/';

var data = {
    countries: {
        listItem: "div.allcountries ul li",
        data: {
            title:"a",
            url: {
                selector: "a",
                attr: "href",
                convert: function(x){
                    return 'http://en.ostranah.ru/' + x;
                }
            }
        }
    }
}

    function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
        page = scrapeIt.scrapeHTML(body, data);
        jsonfile.writeFile(file, page, { spaces: 2 }, function (err) {
            console.error(err || 'success')
        });
    }
}

request(options, callback);
