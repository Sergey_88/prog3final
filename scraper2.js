const request = require('request');
const scrapeIt = require('scrape-it');
const jsonfile = require('jsonfile');
var file = 'data.json';
var inf ={List:[]};
var maindir = "div.info dl ";
jsonfile.readFile(file, function (err, obj) {
    console.dir(err || 'success Read Json');
    scraperloop(obj.countries, 0);
});
var i = 1;
var data = {
    num:{
        selector:"-",
        convert: function () {
            return i++;
        }
    },
    Country:"article h1",
    Capital:maindir + "dt:contains('Capital') + dd",
    Area: {
        selector: maindir + "dt:contains('Area') + dd",
           convert: function(x){
               return parseFloat(((x.split("in the world").pop()).replace(/\s/g, '')).replace(",","."));
            }
    },
    Population: {
        selector: maindir + "dt:contains('Population, ppl.') + dd",
           convert: function(x){
               
               return parseFloat(((x.split("in the world").pop()).replace(/\s/g, '')).replace(",","."));
            }
    },
    Populationgrowth: {
        selector: maindir + "dt:contains('Population growth') + dd",
        convert: function (x) {
            return parseFloat((((x.split("in the world").pop()).replace(/%.*/, "")).replace(/\s/g, '')).replace(",","."));
        }
    },
    Averagelife: {
        selector: maindir + "dt:contains('Average life') + dd",
        convert: function (x) {
            return parseFloat(((x.replace(/\(.*/, "")).replace(/\s/g, '')).replace(",","."));
        }
    },
    Populationdensity: {
        selector: maindir + "dt:contains('Population density') + dd",
        convert: function (x) {
            return parseFloat(((x.split("in the world").pop()).replace(/\s/g, '')).replace(",","."));
        }
    },
    Officiallanguages: {
        selector: maindir + "dt:contains('Official language') + dd",
           convert: function(x){
               return (x.replace(/\s/g, '')).split(",");
            }
    },
    Accesstotheseas: {
        selector: maindir + "dt:contains('Access to the seas and oceans') + dd",
           convert: function(x){
                if(x != "no") return "yes";
                else return "no";

            }
    },
    
}

function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
        page = scrapeIt.scrapeHTML(body,data);
        inf.List.push(page);
        jsonfile.writeFile('List.json', inf, { spaces: 2}, function (err) {
            console.error(err || 'success Write in Json')
        });
    }
}

function scraperloop(arr, i) {
    setTimeout(function () {
        if(i < arr.length)
        var url = arr[i].url;
        else 
        return 0;
        request(url, callback)
        scraperloop(arr, ++i)
    }, 2000)
}


