google.charts.load('45', {
    'packages': ['table', 'geochart', 'corechart', 'corechart', "line"]
});
google.charts.setOnLoadCallback(drawTable);
google.charts.setOnLoadCallback(drawRegionsMap);
google.charts.setOnLoadCallback(drawO);
google.charts.setOnLoadCallback(drawVisualization);
google.charts.setOnLoadCallback(drawSeriesChart);
google.charts.setOnLoadCallback(drawlife);
google.charts.setOnLoadCallback(drawgrow);
function drawTable() {
    var k = 0;
    var jsonData = $.ajax({
        url: "/List",
        dataType: "json",
        async: false
    }).responseText;
    var jsonData = JSON.parse(jsonData);
    var data = new google.visualization.DataTable(jsonData);

    ArrayofColums = ["Number", "County", "Capital city", "Area/km^2/", "Population", "Population growth/per year/", "Average life expectancy", "Population density/ppl./km2/",  "Access to the seas and oceans"];
    for (var i in jsonData[0]) {
        if (i == "Officiallanguages"){data.addColumn("string", "Official language");continue;}
        data.addColumn(typeof jsonData[0][i], ArrayofColums[k]);
        k++;
    }

    for(var i = 0; i < jsonData.length; i++){
        data.addRow([jsonData[i].num, jsonData[i].Country, jsonData[i].Capital, jsonData[i].Area, jsonData[i].Population, jsonData[i].Populationgrowth, jsonData[i].Averagelife, jsonData[i].Populationdensity, (jsonData[i].Officiallanguages).toString(), jsonData[i].Accesstotheseas]);
    }

    var options = {
        width:'100%',
        height:'100%'

    };
    var table = new google.visualization.Table(document.getElementById('Table'));
    table.draw(data,options);
}

function drawRegionsMap() {

    var jsonData = $.ajax({
        url: "/List",
        dataType: "json",
        async: false
    }).responseText;
    var jsonData = JSON.parse(jsonData);
    var data = new google.visualization.DataTable(jsonData);
   
    data.addColumn("string", "Country");
    data.addColumn("number", "Population");


    for (var i = 0; i < jsonData.length; i++) {
        data.addRow([jsonData[i].Country,jsonData[i].Population]);
    }

    var options = {
        colorAxis: { colors: ['green'] },
        backgroundColor: '#81d4fa',
        datalessRegionColor: '#f8bbd0',
        width:'100%',
        height:'100%'
    };

    var chart = new google.visualization.GeoChart(document.getElementById('maps'));

    chart.draw(data, options);
}

function drawO() {

    var jsonData = $.ajax({
        url: "/List",
        dataType: "json",
        async: false
    }).responseText;
    var jsonData = JSON.parse(jsonData);
    var data = new google.visualization.DataTable(jsonData);

    data.addColumn("string", "Acces to water");
    data.addColumn("number", "yes/no");
    var yes = 0;
    var no = 0;

    for (var i = 0; i < jsonData.length; i++) {
        if(jsonData[i].Accesstotheseas == "yes") yes++;
        if (jsonData[i].Accesstotheseas == "no") no++;
    }
    
    data.addRow(["Have Access to the seas and oceans",yes]);
    data.addRow(["Haven't Access to the seas and oceans", no]);

    var options = {
        title:"What part of Countries have acces to Water",
        height:$("#main").height(),
        width:'100%',
        legend:'center'
    };

    var chart = new google.visualization.PieChart(document.getElementById('ocean'));

    chart.draw(data, options);
}
function drawVisualization() {
    var jsonData = $.ajax({
        url: "/List",
        dataType: "json",
        async: false
    }).responseText;
    var jsonData = JSON.parse(jsonData);
    var data = new google.visualization.DataTable(jsonData);

    data.addColumn("string", "Language");
    data.addColumn("number", "count");

    var countoflang=[];
    for (var i = 0; i < jsonData.length; i++) {
        for (var t=0; t<jsonData[i].Officiallanguages.length;t++){
            countoflang.push(jsonData[i].Officiallanguages[t]);
        }
    }
    countoflang.sort();
    var result ={};
    for (var i = 0; i < countoflang.length; ++i) {
        if (!result[countoflang[i]])
            result[countoflang[i]] = 0;
        ++result[countoflang[i]];
    }
    
    for(var y in result){
        data.addRow([y,result[y]]);
    }


    var options = {

        height:500,
        title:"Languages in The World",
        seriesType: 'bars',
        series: { 5: { type: 'line' } }
    };

    var chart = new google.visualization.ComboChart(document.getElementById('lang'));
    chart.draw(data, options);
}

function drawSeriesChart() {
    var jsonData = $.ajax({
        url: "/List",
        dataType: "json",
        async: false
    }).responseText;
    var jsonData = JSON.parse(jsonData);
    var data = new google.visualization.DataTable(jsonData);

    data.addColumn("string", "Country");
    data.addColumn("number", "Population growth");
    data.addColumn("number", "Area");

    for (var i = 0; i < jsonData.length; i++) {
        data.addRow([jsonData[i].Country, jsonData[i].Populationgrowth, jsonData[i].Area]);
    }


    var options = {
        height: 500,
        title:"Conect between Population growth Area",
        sizeAxis: { maxSize: 10},
        colors: ['green']
    };

    var chart = new google.visualization.BubbleChart(document.getElementById('area'));
    chart.draw(data, options);
}

function drawlife() {
    var jsonData = $.ajax({
        url: "/List",
        dataType: "json",
        async: false
    }).responseText;
    var jsonData = JSON.parse(jsonData);
    var data = new google.visualization.DataTable(jsonData);

    data.addColumn("string", "Country");
    data.addColumn("number", "Average life");

    for (var i = 0; i < jsonData.length; i++) {
        data.addRow([jsonData[i].Country, jsonData[i].Averagelife]);
    }

    var options = {
        title: 'Average life Of People',
        height: 500, 
        colors: ['orange']
    };

     var chart = new google.visualization.LineChart(document.getElementById('life'));
     chart.draw(data, options);
    
}

function drawgrow() {
    var jsonData = $.ajax({
        url: "/List",
        dataType: "json",
        async: false
    }).responseText;
    var jsonData = JSON.parse(jsonData);
    var data = new google.visualization.DataTable(jsonData);

    data.addColumn("string", "Country");
    data.addColumn("number", "Population density");

    for (var i = 0; i < jsonData.length; i++) {
        data.addRow([jsonData[i].Country, jsonData[i].Populationdensity]);
    }

    var options = {
        height: 500,
        title: 'Population density',
        colors: ['red']
    };

    var chart = new google.visualization.AreaChart(document.getElementById('grow'));
    chart.draw(data, options);
}